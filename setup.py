# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='Micro Step Analysis',
    version='0.1.0',
    description='Sample package for Python-Guide.org',
    long_description=readme,
    author='Lorenz Steber',
    author_email='lorenz.steber@gmail.com',
    url='https://gitlab.lrz.de/hm-steber/micro_step_analysis.git',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

