import os

import plotly.graph_objects as go

from msa_util.file_utils.path_getters import get_vadere_scenario_path, get_project_path
from msa_util.helpers import print_error
from msa_util.msa_exceptions import ScriptError
from msa_util.plotting.complete_plotting import add_topography, add_error_trajectories, add_trajectories
from msa_util.simulation_utils import run_simulation, get_data_from_simulation
from msa_util.trajectories_utils.error_trajectory import ErrorTrajectory
from msa_util.trajectories_utils.trajectory import Scenario

"""This file is only meant for testing purposes"""

if __name__ == '__main__':
    output_path = ''
    try:
        scenario_path = get_vadere_scenario_path(['TestingModelStepComparator', 'scenarios',
                                                  'test_model_step_comperator_osm.scenario'])
        scenario_path2 = os.path.join(get_project_path(),
                                      'scenarios/scenarios/two_peds_crossing_smoothed_msc_osm.scenario')
        Scenario.from_scenario(scenario_path2)
        output_path = run_simulation(scenario_path2, number_of_runs=1, sim_step_length=0.2)
        for dirpath, dirnames, filenames in os.walk(output_path):
            print(dirpath, dirnames, filenames)
    except ScriptError as e:
        print_error(e)
        exit(e)

    trays, trays_sim, errors = get_data_from_simulation('/Users/q494729/IdeaProjects/micro_step_analysis/scenarios/output/two_peds_crossing_smoothed_msc_osm_2020-07-17_11-58-33.796')
    traj_sim_df = trays_sim[0]

    error_trajs = ErrorTrajectory.create_multi_from_df(traj_sim_df)
    fig = go.Figure()
    add_topography(fig, scenario_path2)
    add_trajectories(fig, error_trajs)
    add_error_trajectories(fig, error_trajs)
    fig.show()
