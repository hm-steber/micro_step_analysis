import pandas as pd
import pytest

from msa_util.names import DEFAULT_HEADER, DEFAULT_MEAN_SIM_HEADER
from msa_util.trajectories_utils.error_trajectory import ErrorScenario, ErrorTrajectory
from msa_util.trajectories_utils.trajectory import Scenario, Trajectory


class TestTraj:
    traj_df = df = pd.DataFrame([[1, 0, 1, 0, 0, 0, 1],
                                 [2, 0, 1, 1, 1, 2, 1],
                                 [1, 1, 2, 0, 1, 0, 2],
                                 [2, 1, 2, 2, 1, 4, 1]], columns=DEFAULT_HEADER)

    def test_create_scenario(self):
        scenario = Scenario.from_trajectories('', trajectories=Trajectory.create_multi_from_df(self.traj_df))
        assert scenario.get_ids() == [1, 2]

    def test_traj_functions(self):
        scenario = Scenario.from_trajectories('', trajectories=Trajectory.create_multi_from_df(self.traj_df))
        traj = scenario.get_part(1).trajectories[0]

        assert traj.first_appearance() == 0.0
        assert traj.last_appearance() == 2.0
        assert traj.get_average_speed() == 1.0
        assert traj.get_length() == 2.0
        assert traj.get_time_length() == 2.0

    def test_traj_get_cords(self):
        scenario = Scenario.from_trajectories('', trajectories=Trajectory.create_multi_from_df(self.traj_df))
        traj = scenario.get_part(2).trajectories[0]
        x, y = traj.get_x_y()
        assert x == [1, 2, 4]
        assert y == [1, 1, 1]

    def test_corrupt_traj(self):
        corrupt_traj_df = pd.DataFrame([[1, 0, 1, 0, 0, 0, 1],
                                        [2, 0, 1, 1, 1, 2, 1],
                                        [2, 0, 1, 1, 1, 2, 1],
                                        [1, 1, 2, 0, 1, 0, 2],
                                        [2, 1, 2, 2, 1, 4, 1]], columns=DEFAULT_HEADER)
        with pytest.raises(Exception):
            Scenario.from_trajectories('', trajectories=Trajectory.create_multi_from_df(corrupt_traj_df))


class TestErrorTraj:
    traj_df = df = pd.DataFrame([[1, 0, 1, 0, 0, 0, 1, 0, 0, 1.5, 0],
                                 [1, 1, 2, 0, 1, 0, 2, 0, 0, 2.5, 0]], columns=DEFAULT_MEAN_SIM_HEADER)

    def test_create_scenario(self):
        scenario = ErrorScenario.from_trajectories('', trajectories=[
            ErrorTrajectory.create_from_single_line_df(self.traj_df)])
        assert scenario.get_ids() == [1]

    def test_traj_functions(self):
        scenario = ErrorScenario.from_trajectories('', trajectories=[
            ErrorTrajectory.create_from_single_line_df(self.traj_df)])
        traj = scenario.get_part(1).trajectories[0]

        assert traj.first_appearance() == 0.0
        assert traj.last_appearance() == 2.0
        assert traj.get_average_speed() == 1.0
        assert traj.get_length() == 2.0
        assert traj.get_time_length() == 2.0

    def test_get_error_with_cords(self):
        scenario = ErrorScenario.from_trajectories('', trajectories=[
            ErrorTrajectory.create_from_single_line_df(self.traj_df)])
        assert scenario.get_errors_with_cords() == ([0, 0], [0, 1], [0.5, 0.5])

    def test_corrupt_traj(self):
        corrupt_traj_df = pd.DataFrame([[1, 0, 1, 0, 0, 0, 1, 0, 0, 1.5, 0],
                                        [1, 0, 1, 0, 0, 0, 1, 0, 0, 1.5, 0],
                                        [1, 1, 2, 0, 1, 0, 2, 0, 0, 2.5, 0]], columns=DEFAULT_MEAN_SIM_HEADER)
        with pytest.raises(Exception):
            ErrorScenario.from_trajectories('',
                                            trajectories=[ErrorTrajectory.create_from_single_line_df(corrupt_traj_df)])

    def test_get_error_trajs(self):
        scenario = ErrorScenario.from_trajectories('', trajectories=[
            ErrorTrajectory.create_from_single_line_df(self.traj_df)])
        traj = scenario.trajectories[0]
        assert traj.get_error_x_y() == ([0, 0, None, 0, 0, None], [0, 1.5, None, 1, 2.5, None])
