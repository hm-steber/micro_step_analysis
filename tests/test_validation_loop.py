from msa_util.file_utils.path_getters import get_temp_dir, get_project_path
import os
from os import listdir

from msa_util.file_utils.scenario_utils import get_scenario_dict
from msa_util.simulation_utils import simulate_and_create_msa_data
from msa_util.statisical_analysis.metric import calc_metric
from msa_util.trajectories_utils.error_trajectory import ErrorTrajectory


def test_validation_loop() -> None:
    scenario_path = os.path.join(get_project_path(), 'scenarios/scenarios/t_junction_240_50_smoothed_msc_osm.scenario')
    print(scenario_path)
    output_path = get_temp_dir('test_output_folder')

    result_files_path = simulate_and_create_msa_data(scenario_path=scenario_path, output_path=output_path, number_of_runs=5)
    assert output_path == result_files_path
    assert len(listdir(output_path)) == 3

    mean_error_file = os.path.join(output_path, 'mean_error.csv')
    mean_traj_file = os.path.join(output_path, 'mean_traj_data.csv')
    scenario_file = os.path.join(output_path, 'scenario_file.scenario')

    # check scenario file
    scenario_dict = get_scenario_dict(scenario_file)
    assert scenario_dict['scenario']['attributesSimulation']['realTimeSimTimeRatio'] == 0.0
    assert scenario_dict['scenario']['attributesSimulation']['visualizationEnabled'] is False
    assert scenario_dict['scenario']['attributesSimulation']['useFixedSeed'] is False

    # check trajectories by getting tested for continuity by Trajectory.validate() during creation
    ErrorTrajectory.create_list_from_csv(mean_traj_file)

    # calc metric
    assert 0.5 < calc_metric(mean_error_file) < 1.0





