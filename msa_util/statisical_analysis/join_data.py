from typing import List

import numpy as np
import pandas as pd

from msa_util.names import ERROR, DATA_TYPE, X_END, Y_END, ERROR_MEAN, ERROR_VAR


def join_error_dfs(error_dfs: List[pd.DataFrame]):
    df_err = error_dfs[0]
    err = np.ones(df_err.shape[0]).reshape([-1, 1])
    for tmp_df in error_dfs:
        err_tmp = tmp_df[ERROR].values.reshape([-1, 1])
        err = np.append(err, err_tmp, axis=1)
    err = err[:, 1:]
    err_mean = np.mean(err, axis=1)
    err_var = np.var(err, axis=1)
    df_err = df_err.drop(columns=[ERROR])
    df_err.loc[:, ERROR_MEAN] = err_mean
    df_err.loc[:, ERROR_VAR] = err_var
    return df_err


def join_sim_dfs(sim_dfs: List[pd.DataFrame]) -> pd.DataFrame:
    res = sim_dfs[0][sim_dfs[0][DATA_TYPE] == 0].copy()
    for cord in [X_END, Y_END]:
        xs = np.ones(res.shape[0]).reshape([-1, 1])
        for tmp_df in sim_dfs:
            x_tmp = tmp_df[tmp_df[DATA_TYPE] == 1][cord].values.reshape([-1, 1])
            xs = np.append(xs, x_tmp, axis=1)
        xs = xs[:, 1:]
        x_mean = np.mean(xs, axis=1)
        x_var = np.var(xs, axis=1)
        res.loc[:, f'{cord}Mean'] = x_mean
        res.loc[:, f'{cord}Var'] = x_var
    res = res.drop(columns=[DATA_TYPE])
    return res
