import numpy as np
import pandas as pd

from msa_util.names import ERROR_MEAN, ERROR_VAR, PED_ID, TIME_START


def calc_metric(error_data_path: str) -> float:
    error_df = pd.read_csv(error_data_path, sep=' ')
    return calc_metric_from_df(error_df)


def calc_metric_from_df(error_df):
    errors = error_df[ERROR_MEAN].values
    var = error_df[ERROR_VAR].values
    var = np.sqrt(var)
    metric = (2 * errors + var)
    metric = np.mean(np.square(metric))
    return float(metric)


def calc_metric_without_first(error_data_path: str) -> float:
    error_df = pd.read_csv(error_data_path, sep=' ')

    ped_dfs = []
    for ped_id in error_df[PED_ID].unique():
        ped_df = (error_df[error_df[PED_ID] == ped_id])
        ped_df = ped_df.sort_values(TIME_START).reset_index()
        ped_df = ped_df.iloc[1:]
        ped_dfs.append(ped_df)

    return calc_metric_from_df(pd.concat(ped_dfs))


if __name__ == '__main__':
    t_junc_exp_path = '/Users/q494729/IdeaProjects/micro_step_analysis/example_data/t_junction_smoothed_osm_100/t_junction_mean_error.csv'
    t_junc_random_seed_path = '/Users/q494729/IdeaProjects/micro_step_analysis/example_data/metric_testing/msa_osm_sim_random_seed/mean_error.csv'
    t_junc_fixed_seed_path = '/Users/q494729/IdeaProjects/micro_step_analysis/example_data/metric_testing/msa_osm_sim_fixed_seed/mean_error.csv'
    print(calc_metric(t_junc_exp_path))
    print(calc_metric(t_junc_random_seed_path))
    print(calc_metric(t_junc_fixed_seed_path))
    print('')
    print(calc_metric_without_first(t_junc_exp_path))
    print(calc_metric_without_first(t_junc_random_seed_path))
    print(calc_metric_without_first(t_junc_fixed_seed_path))
