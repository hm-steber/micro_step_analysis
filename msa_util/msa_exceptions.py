class ScriptError(Exception):
    """Exception which message will be shown to user and ends the program.
    Is supposed to be thrown in case of user input error"""
    pass
