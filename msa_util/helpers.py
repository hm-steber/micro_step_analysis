import json
import sys
from typing import Tuple


def print_error(err: Exception) -> None:
    print(f'{type(err).__name__}: {err}')
    sys.exit()


def print_dict(to_print: dict) -> None:
    print(json.dumps(to_print, indent=4))


def progress_bar(iterable, prefix='Progress', suffix='Complete', decimals=1, length=100, fill='█', print_end="\r"):
    total = len(iterable)

    def print_progress_bar(iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filled_length = int(length * iteration // total)
        bar = fill * filled_length + '-' * (length - filled_length)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=print_end)

    print_progress_bar(0)
    for i, item in enumerate(iterable):
        yield item
        print_progress_bar(i + 1)
    print()
