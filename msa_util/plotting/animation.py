import os
from typing import List
import numpy as np

import plotly.graph_objects as go

from msa_util.file_utils.path_getters import get_project_path
from msa_util.names import TIME_START, X_START, X_SIM, Y_START, Y_SIM
from msa_util.plotting.complete_plotting import add_topography
from msa_util.trajectories_utils.error_trajectory import ErrorScenario, ErrorTrajectory


def add_error_frames(fig: go.Figure, error_trajectories: ErrorScenario, time_steps: float) -> go.Figure:
    error_frames = []
    sim_df = error_trajectories.to_df()
    sim_max_time = sim_df[TIME_START].max()
    for t in np.arange(0, sim_max_time + time_steps, time_steps):
        tmp_df = sim_df[(sim_df[TIME_START] >= t) & (sim_df[TIME_START] < t + time_steps)]
        x_values = []
        y_values = []
        for i, fs in tmp_df.iterrows():
            x_values.extend([fs[X_START], fs[X_SIM], None])
            y_values.extend([fs[Y_START], fs[Y_SIM], None])
        if len(x_values) > 0:
            error_frames.append(go.Frame(
                data=go.Scatter(
                    x=x_values,
                    y=y_values,
                )
            )
            )

    fig_dict = fig.to_dict()
    fig_dict['frames'] = error_frames
    fig_dict['data'].append(fig_dict['data'][0])
    fig_dict['layout']['updatemenus'] = [
        {
            'buttons': [
                {
                    'args': [None, {'frame': {'duration': 1000, 'redraw': False},
                                    'fromcurrent': True, 'transition': {'duration': 1000, 'easing': 'quadratic-in-out'}}],
                    'label': 'Play',
                    'method': 'animate'
                },
                {
                    'args': [[None], {'frame': {'duration': 0, 'redraw': False}, 'mode': 'immediate',
                                      'transition': {'duration': 0}}],
                    'label': 'Pause',
                    'method': 'animate'
                }
            ],
            'direction': 'left',
            'pad': {'r': 10, 't': 87},
            'showactive': False,
            'type': 'buttons',
            'x': 0.1,
            'xanchor': 'right',
            'y': 0,
            'yanchor': 'top'
        }
    ]
    fig = go.Figure(fig_dict)

    return fig


def animate_error_scenario(error_trajectories: ErrorScenario, time_steps: float, name: str = 'Error Scenario Animation') -> go.Figure:
    """Not working"""
    fig = go.Figure()
    add_topography(fig, error_trajectories.scenario_path, name=name)
    add_error_frames(fig, error_trajectories, time_steps)
    return fig


if __name__ == '__main__':
    scenario_path = os.path.join(get_project_path(), 'example_data/t_junction_smoothed_osm_100/scenario_file.scenario')
    data_path = os.path.join(get_project_path(), 'example_data/t_junction_smoothed_osm_100/t_junction_mean_data.csv')
    traj_list = ErrorTrajectory.create_list_from_csv(csv_path=data_path)
    err_scenario = ErrorScenario.from_trajectories(scenario_path=scenario_path, trajectories=traj_list)
    animate_error_scenario(err_scenario, 0.5)
