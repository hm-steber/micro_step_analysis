import os
from typing import List

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import seaborn as sns
from plotly.subplots import make_subplots
from scipy import interpolate
from sklearn.neighbors import KNeighborsRegressor

from msa_util.file_utils.path_getters import get_project_path
from msa_util.file_utils.scenario_utils import get_scenario_dict
from msa_util.names import PED_ID, TIME_START, ERROR_MEAN, ERROR_VAR, X_START, Y_START
from msa_util.plotting.complete_plotting import add_topography, add_trajectories
from msa_util.simulation_utils import get_data_from_simulation
from msa_util.trajectories_utils.error_trajectory import ErrorTrajectory, ErrorScenario


def add_error_trajectories(fig: go.Figure, error_trajectories: List[ErrorTrajectory], start_time: float = 0,
                           end_time: float = 10000) -> go.Figure:
    traces = []
    for i, trajectory in enumerate(error_trajectories):
        name = trajectory.name or f'Errorsteps Pedestrian{trajectory.id}'
        color = 'red'
        x, y = trajectory.get_error_x_y(start_time, end_time)
        traces.append(go.Scatter(
            x=x,
            y=y,
            name=name,
            fillcolor=color,
            mode='lines'
        ))
    fig.add_traces(traces)
    return fig


def plot_error_scenario_from_output_path(output_path: str, scale: float = 1, name: str = None):
    trays, trays_sim, errors, scenario_paths = get_data_from_simulation(output_path)
    traj_sim_df = trays_sim[0]
    scenario_path = scenario_paths[0]

    error_trajs = ErrorTrajectory.create_multi_from_df(traj_sim_df)
    return plot_error_scenario_from_trajectories(error_trajs, name=name, scale=scale, scenario_path=scenario_path)


def plot_error_scenario(scenario: ErrorScenario, scale: float = 1, name: str = None, start_time: float = 0,
                        end_time: float = 10000) -> go.Figure:
    return plot_error_scenario_from_trajectories(scenario.trajectories, scenario.scenario_path, name=name, scale=scale,
                                                 start_time=start_time, end_time=end_time)


def plot_error_scenario_from_trajectories(error_trajs: List[ErrorTrajectory], scenario_path: str, scale: float = 1,
                                          name: str = None, start_time: float = 0,
                                          end_time: float = 10000) -> go.Figure:
    fig = go.Figure()
    add_topography(fig, scenario_path, name=name, scale=scale)
    add_trajectories(fig, error_trajs, start_time=start_time, end_time=end_time)
    add_error_trajectories(fig, error_trajs, start_time=start_time, end_time=end_time)
    fig.show()
    return fig


def plot_mean_ped_error_and_var(csv_path: str) -> go.Figure:
    comp_err_df = pd.read_csv(csv_path, delimiter=' ')
    comp_err_df[ERROR_VAR] = comp_err_df[ERROR_VAR].apply(lambda x: np.sqrt(x))

    # create list of ped dfs, which time is normed to 100 and first and last rows are removed
    ped_dfs = []
    for ped_id in comp_err_df[PED_ID].unique():
        temp_ped_df = comp_err_df[comp_err_df[PED_ID] == ped_id]
        temp_ped_df = temp_ped_df.sort_values(TIME_START).reset_index()
        temp_ped_df = temp_ped_df[1:-1]
        temp_ped_df[TIME_START] = temp_ped_df[TIME_START] - temp_ped_df[TIME_START].min()
        temp_ped_df[TIME_START] = temp_ped_df[TIME_START] * (100 / temp_ped_df[TIME_START].iloc[-1])
        temp_ped_df = temp_ped_df.round(6)
        ped_dfs.append(temp_ped_df)

    # creates interpolated functions
    err_functions = []
    err_var_functions = []
    for ped_df in ped_dfs:
        x = ped_df[TIME_START]
        y = ped_df[ERROR_MEAN]
        err_functions.append(interpolate.interp1d(x, y, kind='cubic'))
        y = ped_df[ERROR_VAR]
        err_var_functions.append(interpolate.interp1d(x, y, kind='cubic'))

    temp_err = []
    temp_var = []
    x = np.linspace(0, 100, 100)
    for f_err, f_var in zip(err_functions, err_var_functions):
        temp_err.append(f_err(x))
        temp_var.append(f_var(x))

    # averages err and var over all pedestrians
    mean_err_array = np.array(temp_err).mean(axis=0)
    mean_var_array = np.array(temp_var).mean(axis=0)

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(
        go.Scatter(x=x, y=mean_err_array, name="Mean error"),
        secondary_y=False,
    )
    fig.add_trace(
        go.Scatter(x=x, y=mean_var_array, name="Mean standard deviation"),
        secondary_y=True,
    )

    fig.update_layout(
        title_text="Mean error and error deviation over all pedestrians over their traveled way"
    )
    fig.update_xaxes(title_text="Traveled way in %")
    fig.update_yaxes(title_text="Error [m]", secondary_y=False)
    fig.update_yaxes(title_text="Standard deviation [m]", secondary_y=True)
    return fig


def plot_error_heatmap_k_neighbors(scenario: ErrorScenario, n_neighbor: int = 5, name: str = '', label_name: str = ''):
    scenario_path = scenario.scenario_path
    x, y, z = scenario.get_errors_with_cords()
    return plot_heatmap_with_k_nearest(n_neighbor, scenario_path, x, y, z, name, label_name)


def plot_heatmap_with_k_nearest(n_neighbor, scenario_path, x, y, z, name: str = '', label_name: str = ''):
    cords = np.array([x, y]).transpose()
    neigh = KNeighborsRegressor(n_neighbors=n_neighbor, weights='distance')
    neigh.fit(cords, z)
    scenario_dict = get_scenario_dict(scenario_path)
    topography_bounds_dict = scenario_dict['scenario']['topography']['attributes']['bounds']
    x, y = np.meshgrid(
        np.linspace(topography_bounds_dict['x'], topography_bounds_dict['width'] + topography_bounds_dict['x'], 1000),
        np.linspace(topography_bounds_dict['y'], topography_bounds_dict['height'] + topography_bounds_dict['y'], 1000))
    new_cords = np.vstack([x.ravel(), y.ravel()]).transpose()
    z = neigh.predict(new_cords)
    z = np.nan_to_num(z)
    fig = go.Figure()
    fig.update_layout(
        title=name or f'testheatmap n={n_neighbor}',
        xaxis_title="x[cm]",
        yaxis_title="y[cm]",
        width=topography_bounds_dict['width'] * 100,
        height=topography_bounds_dict['height'] * 100,
    )
    fig.update(data=go.Heatmap(
        z=z.reshape(x.shape),
        colorbar=dict(title=label_name or 'Error [m]')
    ))
    return fig


def plot_error_var_heatmap(data_path: str, err_path: str, scenario_path: str, n_neighbor: int = 5, name: str = '', label_name: str = ''):
    data_df = pd.read_csv(data_path, sep=' ')
    error_df = pd.read_csv(err_path, sep=' ')
    join_data_dfs = []
    for ped_id in data_df[PED_ID].unique():
        temp_err_df = error_df[error_df[PED_ID] == ped_id].reset_index(drop=True)
        temp_data_df = data_df[data_df[PED_ID] == ped_id].reset_index(drop=True)
        join_data_dfs.append(temp_data_df.join(temp_err_df[[ERROR_MEAN, ERROR_VAR]]))
    err_data_df = pd.concat(join_data_dfs)
    x = err_data_df[X_START].tolist()
    y = err_data_df[Y_START].tolist()
    z = err_data_df[ERROR_VAR].apply(np.sqrt).tolist()
    return plot_heatmap_with_k_nearest(n_neighbor, scenario_path, x, y, z, name, label_name)


def plot_traj_density(scenario: ErrorScenario):
    x, y, _ = scenario.get_errors_with_cords()
    fig = sns.jointplot(x=x, y=y, kind="kde")
    fig.set_axis_labels('x [m]', 'y [m]')
    return fig


if __name__ == '__main__':


    scenario_path = os.path.join(get_project_path(), 'example_data/t_junction_smoothed_osm_100/scenario_file.scenario')
    data_path = os.path.join(get_project_path(), 'example_data/t_junction_smoothed_osm_100/t_junction_mean_data.csv')
    error_path = os.path.join(get_project_path(), 'example_data/t_junction_smoothed_osm_100/t_junction_mean_error.csv')

    # plot var
    #fig = plot_error_var_heatmap(data_path, error_path, scenario_path, 20, 'T-Junction Standardabweichung Heatmap', 'SD [m]')
    #fig.write_image("/Users/q494729/IdeaProjects/bachelor-arbeit-msa/pictures/visualization/standard_deviation_heatmap.jpeg")

    traj_list = ErrorTrajectory.create_list_from_csv(csv_path=data_path)
    err_scenario = ErrorScenario.from_trajectories(scenario_path=scenario_path, trajectories=traj_list)
    plot_traj_density(err_scenario)
    # plot error
    #fig = plot_error_heatmap_k_neighbors(err_scenario, 20, 'T-Junction Fehler Heatmap', 'Error [m]')
    #fig.write_image("/Users/q494729/IdeaProjects/bachelor-arbeit-msa/pictures/visualization/error_heatmap.jpeg")
