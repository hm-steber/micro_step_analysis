from typing import List

import plotly.graph_objects as go

from msa_util.file_utils.scenario_utils import get_scenario_dict
from msa_util.trajectories_utils.error_trajectory import ErrorTrajectory, ErrorScenario
from msa_util.trajectories_utils.trajectory import Trajectory, Scenario


def add_topography(fig: go.Figure, scenario_file: str, name: str = None, scale: float = 1) -> go.Figure:
    scenario_dict = get_scenario_dict(scenario_file)

    topography_bounds_dict = scenario_dict['scenario']['topography']['attributes']['bounds']
    obs_layout_list = []

    obstacle_list = scenario_dict['scenario']['topography']['obstacles']
    extract_object(obs_layout_list, obstacle_list, 'Gray')

    obstacle_list = scenario_dict['scenario']['topography']['targets']
    extract_object(obs_layout_list, obstacle_list, 'Orange')

    obstacle_list = scenario_dict['scenario']['topography']['sources']
    extract_object(obs_layout_list, obstacle_list, 'rgb(0, 255, 0)')

    fig.update_layout(
        title=name or scenario_dict['name'],
        xaxis_title="x[m]",
        yaxis_title="y[m]",
        width=topography_bounds_dict['width'] * 100 * scale,
        height=topography_bounds_dict['height'] * 100 * scale,
        xaxis=dict(
            range=[topography_bounds_dict['x'], topography_bounds_dict['width'] + topography_bounds_dict['x']],
            autorange=False,
            zeroline=False,
        ),
        yaxis=dict(
            range=[topography_bounds_dict['y'], topography_bounds_dict['height'] + topography_bounds_dict['y']],
            autorange=False,
            zeroline=False,
        ),
        shapes=obs_layout_list,
    )
    return fig


def extract_object(obs_layout_list, obstacle_list, color):
    for obstacle in obstacle_list:
        obstacle = obstacle['shape']
        if obstacle['type'] != 'RECTANGLE':
            continue
        obs_layout_list.append(dict(
            fillcolor=color,
            line_color=color,
            type='rect',
            x0=obstacle['x'],
            y0=obstacle['y'],
            x1=(obstacle['width'] + obstacle['x']),
            y1=(obstacle['height'] + obstacle['y']),
        ))


def add_trajectories(fig: go.Figure, trajectories: List[Trajectory] or Trajectory, names: List[str] = None, start_time: float = 0, end_time: float = 10000) -> go.Figure:
    if isinstance(trajectories, Trajectory):
        trajectories = [trajectories]

    traces = []
    for i, trajectory in enumerate(trajectories):
        name = names[i] if names else trajectory.name or f'Pedestrian{trajectory.id}'
        x, y = trajectory.get_x_y(start_time, end_time)

        traces.append(go.Scatter(
            x=x,
            y=y,
            name=name,
            text=f'{round(trajectory.first_appearance(), 1)}s - {round(trajectory.last_appearance(), 1)}s'
        ))
    fig.add_traces(traces)
    return fig


def plot_scenario_from_file(scenario_file: str) -> go.Figure:
    scenario_dict = get_scenario_dict(scenario_file)

    data_path = scenario_dict['scenario']['attributesModel']['org.vadere.state.attributes.models.AttributesERM'][
        'experimentDataFilePath']
    fig = go.Figure()
    add_topography(fig, scenario_file)
    add_trajectories(fig, Trajectory.create_from_csv(data_path))
    return fig


def plot_scenario(scenario: Scenario, scale: float = 1, name: str = None):
    fig = go.Figure()
    add_topography(fig, scenario.scenario_path, name=name, scale=scale)
    add_trajectories(fig, scenario.trajectories)
    fig.show()
    return fig


if __name__ == '__main__':
    scenario_path = '/Users/q494729/IdeaProjects/micro_step_analysis/example_data/t_junction_smoothed_osm_100/scenario_file.scenario'
    data_path = '/Users/q494729/IdeaProjects/micro_step_analysis/example_data/t_junction_smoothed_osm_100/t_junction_mean_data.csv'

    traj_list = ErrorTrajectory.create_list_from_csv(csv_path=data_path)
    err_scenario = ErrorScenario.from_trajectories(scenario_path=scenario_path, trajectories=traj_list).get_part([2,3,4,5])

    fig = go.Figure()
    add_topography(fig, scenario_path)
    add_trajectories(fig, err_scenario)
    fig.show()
