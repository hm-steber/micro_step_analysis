import plotly.graph_objects as go
from ipywidgets import widgets

from msa_util.file_utils.scenario_utils import get_scenario_dict
from msa_util.plotting.complete_error_plotting import add_error_trajectories
from msa_util.plotting.complete_plotting import add_topography, add_trajectories
from msa_util.trajectories_utils.error_trajectory import ErrorTrajectory, ErrorScenario


def animate_time_span_error_scenario(scenario_path: str, traj_data_path: str):
    """Use in jupyter notebook"""
    scenario_dict = get_scenario_dict(scenario_path)
    traj_list = ErrorTrajectory.create_list_from_csv(csv_path=traj_data_path)
    err_scenario = ErrorScenario.from_trajectories(scenario_path=scenario_path, trajectories=traj_list)

    default_start_time = 0.0
    default_span = scenario_dict["scenario"]["attributesSimulation"]["simTimeStepLength"] * 1.5
    g = go.FigureWidget()
    add_topography(g, scenario_path, name='Error Timespan Animation')
    add_trajectories(g, err_scenario.trajectories, start_time=default_start_time, end_time=default_start_time+default_span)
    add_error_trajectories(g, err_scenario.trajectories, start_time=default_start_time, end_time=default_start_time+default_span)

    start_time_slider = widgets.FloatSlider(
        value=0.0,
        min=0.0,
        max=scenario_dict["scenario"]["attributesSimulation"]["finishTime"],
        step=scenario_dict["scenario"]["attributesSimulation"]["simTimeStepLength"],
        description='Starttime:',
        continuous_update=False
    )
    start_time_slider.layout.width = '800px'

    def response(change):
        with g.batch_update():
            new_start_time = start_time_slider.value
            g.data = []
            add_trajectories(g, err_scenario.trajectories, start_time=new_start_time, end_time=new_start_time+default_span)
            add_error_trajectories(g, err_scenario.trajectories, start_time=new_start_time, end_time=new_start_time+default_span)

    start_time_slider.observe(response, names="value")

    container = widgets.HBox(children=[start_time_slider])
    return widgets.VBox([container, g])


def animate_pedestrian_span_error_scenario(scenario_path: str, traj_data_path: str):
    """Use in jupyter notebook"""
    scenario_dict = get_scenario_dict(scenario_path)
    traj_list = ErrorTrajectory.create_list_from_csv(csv_path=traj_data_path)
    err_scenario = ErrorScenario.from_trajectories(scenario_path=scenario_path, trajectories=traj_list)

    g = go.FigureWidget()
    add_topography(g, scenario_path, name='Error Pedestrian Span Animation')
    add_trajectories(g, err_scenario.trajectories)
    add_error_trajectories(g, err_scenario.trajectories)

    ped_ids = err_scenario.get_ids()

    slider = widgets.IntRangeSlider(
        min=min(ped_ids),
        max=max(ped_ids),
        step=1,
        description='Pedestrians:',
        continuous_update=False,
    )
    slider.layout.width = '800px'

    def response(change):
        with g.batch_update():
            bounds = slider.value
            temp_scenario = err_scenario.get_part(list(range(bounds[0], bounds[1]+1)))
            g.data = []
            add_trajectories(g, temp_scenario.trajectories)
            add_error_trajectories(g, temp_scenario.trajectories)

    slider.observe(response, names="value")

    container = widgets.HBox(children=[slider])
    return widgets.VBox([container, g])


