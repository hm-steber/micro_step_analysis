from msa_util.simulation_utils import get_data_from_simulation
from msa_util.statisical_analysis.join_data import join_error_dfs
import matplotlib.pyplot as plt
from scipy import stats


def plot_error_hist(output_path: str):
    trays, trays_sim, errors, scenario_paths = get_data_from_simulation(output_path)
    err = join_error_dfs(errors)
    for i in err:
        plt.hist(i)
        plt.show()
    k2, p = stats.normaltest(err, axis=1)
    print(p)


def plot_hist(array, file_name: str = None):
    fig, axs = plt.subplots(4, 4, figsize=(8, 6))
    i = 0
    for axis_x in range(4):
        for axis_y in range(4):
            axs[axis_x, axis_y].hist(array[i])
            i += 1

    if file_name:
        plt.savefig(file_name)
    plt.show()
