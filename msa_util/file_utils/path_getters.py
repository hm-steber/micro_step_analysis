import ntpath
import tempfile
import os
from os.path import expanduser
from typing import Optional, List

from msa_util.msa_exceptions import ScriptError


def get_temp_dir(prefix: str = 'msa_output') -> str:
    return tempfile.mkdtemp(prefix=prefix)


def get_config_path() -> Optional[str]:
    """Not implemented"""
    config_path = 'config.yml'
    if os.path.isfile(config_path):
        return config_path
    else:
        return None


def get_project_path() -> str:
    path = expanduser('~')
    path_parts = os.path.dirname(os.path.abspath(__file__)).split(os.sep)
    home = expanduser('~').split(os.sep)
    last = path_parts.index(home[-1])
    path_parts = path_parts[last+1:]
    for part in path_parts:
        path = os.path.join(path, part)
        if part == 'micro_step_analysis':
            break
    return path + '/'


def get_vadere_jar_path(use_project_jar: bool = True) -> str:
    if get_config_path():
        raise NotImplementedError('Config handling not implemented yet')
    if use_project_jar:
        jar_path = os.path.join(get_project_path(), 'files', 'vadere-console.jar')
    else:
        home = expanduser('~')
        jar_path = os.path.join(home, 'IdeaProjects', 'vadere', 'VadereSimulator', 'target', 'vadere-console.jar')

    if os.path.isfile(jar_path):
        return jar_path
    else:
        raise ScriptError(f'No jar file found at path "{jar_path}".')


def get_vadere_scenario_path(scenario_name: str or List[str]) -> str:
    """Tries to find scenario files in vadere project"""
    if get_config_path():
        raise NotImplementedError('Config handling not implemented yet')

    if isinstance(scenario_name, str):
        scenario_name = [scenario_name]

    home = expanduser('~')
    scenario_path = os.path.join(home, 'IdeaProjects', 'vadere', 'VadereScenarios', *scenario_name)

    if os.path.isfile(scenario_path):
        return scenario_path
    else:
        raise ScriptError(f'No scenario file found at path "{scenario_path}".')


def extend_file_name(path: str, extension: str) -> str:
    head, tail = ntpath.split(path)
    name, ending = tail.split('.', 1)
    return os.path.join(head, f'{name}{extension}.{ending}')
