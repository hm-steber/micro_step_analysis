import json
import os
import tempfile

from msa_util.file_utils.path_getters import extend_file_name, get_project_path
from msa_util.msa_exceptions import ScriptError


def store_scenario_file(data: dict, store_path: str):
    try:
        with open(store_path, 'w') as outfile:
            json.dump(data, outfile, indent=4)
    except Exception as e:
        name = data['name']
        raise ScriptError(f'Storing scenario "{name}" under "{store_path}" failed bc of {e}')


def change_scenario_to_temp_file(scenario_path: str, sim_speed: float = 0.0, sim_step_length: float = None) -> str:
    """Adepts scenario file so it can be simulated as fast as possible"""
    tf = tempfile.NamedTemporaryFile(delete=False)
    temp_path = tf.name
    data = get_scenario_dict(scenario_path)

    try:
        data['scenario']['attributesSimulation']['realTimeSimTimeRatio'] = sim_speed
        data['scenario']['attributesSimulation']['visualizationEnabled'] = False
        data['scenario']['attributesSimulation']['useFixedSeed'] = False

        if sim_step_length:
            data['scenario']['attributesSimulation']['simTimeStepLength'] = sim_step_length
    except KeyError as e:
        raise ScriptError(e)

    store_scenario_file(data, temp_path)
    return temp_path


def check_scenario_file(scenario_path: str) -> bool:
    if not os.path.isfile(scenario_path):
        return False
    return True


def get_scenario_dict(scenario_path: str) -> dict:
    if not os.path.isfile(scenario_path):
        raise ScriptError(f'Scenario at path {scenario_path} does not exist')
    try:
        with open(scenario_path) as json_file:
            data = json.load(json_file)
    except Exception as e:
        raise ScriptError(f'Scenario at path {scenario_path} failed to load bc of "{e}"')
    return data


def change_erm_to_msc_osm(scenario_path: str, store_path: str = None):
    scenario_dict = get_scenario_dict(scenario_path)
    default_msc = get_scenario_dict(os.path.join(get_project_path(), 'scenarios/default_model_step_comperator_osm.scenario'))

    data_path = scenario_dict['scenario']['attributesModel']['org.vadere.state.attributes.models.AttributesERM'][
                              'experimentDataFilePath']

    scenario_dict['processWriters'] = default_msc['processWriters']
    scenario_dict['scenario']['mainModel'] = default_msc['scenario']['mainModel']
    scenario_dict['scenario']['attributesModel'] = default_msc['scenario']['attributesModel']

    scenario_dict['scenario']['attributesModel']['org.vadere.state.attributes.models.AttributesERM'][
                  'experimentDataFilePath'] = data_path
    scenario_dict['name'] = scenario_dict['name'] + '_msc_osm'

    if not store_path:
        store_path = extend_file_name(scenario_path, '_msc_osm')
    store_scenario_file(scenario_dict, store_path)


if __name__ == '__main__':
    scenario_path1 = '/Users/q494729/IdeaProjects/micro_step_analysis/scenarios/scenarios/two_peds_crossing_smoothed.scenario'
    scenario_path2 = '/Users/q494729/IdeaProjects/micro_step_analysis/scenarios/scenarios/t_junction_240_50_smoothed.scenario'
    change_erm_to_msc_osm(scenario_path2)
