import json

from msa_util.file_utils.scenario_utils import get_scenario_dict
from msa_util.trajectories_utils.trajectory import Scenario
from msa_util.file_utils.path_getters import extend_file_name


def smooth_scenario(path: str, n: int, name_attachment: str = '_smoothed') -> None:
    """
    Smooths are given scenario and saves it as a copy in the same place.
    :param path: Scenario to smooth path
    :param n: points before and after, over which is evened
    :param name_attachment: str will be attached to the file name
    :return: None
    """
    data = get_scenario_dict(path)

    data['name'] = data['name'] + name_attachment

    # change out datafile
    data_path = data['scenario']['attributesModel']['org.vadere.state.attributes.models.AttributesERM'][
        'experimentDataFilePath']
    new_data_path = extend_file_name(data_path, name_attachment)
    Scenario(path).smooth(n).save(new_data_path)
    data['scenario']['attributesModel']['org.vadere.state.attributes.models.AttributesERM'][
        'experimentDataFilePath'] = new_data_path

    # store new scenario file
    new_scenario_path = extend_file_name(path, name_attachment)
    with open(new_scenario_path, 'w') as outfile:
        json.dump(data, outfile)
