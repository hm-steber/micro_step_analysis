import json
import os
import click

import pandas as pd
import ntpath

from msa_util.file_utils.path_getters import get_project_path
from msa_util.names import TIME_START, TIME_END, X_START, Y_START, X_END, Y_END, PED_ID

TIME = 'time'
X = 'x'
Y = 'y'
TIME_FACTOR_TO_SEC = 1.0 / 30
STEPS = 1


def change_df_to_traj(df: pd.DataFrame) -> {}:
    return {
        PED_ID: df[PED_ID].iloc[0],
        TIME_START: df[TIME].iloc[0],
        TIME_END: df[TIME].iloc[-1],
        X_START: df[X].iloc[0],
        Y_START: df[Y].iloc[0],
        X_END: df[X].iloc[-1],
        Y_END: df[Y].iloc[-1]
    }


def create_scenario_file(max_time: int, max_x: float, max_y: float, store_path: str, target_cords: [] = None) -> None:
    head, tail = ntpath.split(store_path)
    file_name = os.path.join('scenarios', str(tail.split('.')[0]) + '.scenario')
    new_path = os.path.join(head, file_name)

    default_scenario_path = os.path.join(get_project_path(), 'scenarios/default_experiment_reader.scenario')
    with open(default_scenario_path) as json_file:
        data = json.load(json_file)

    data['name'] = str(tail.split('.')[0])
    data['scenario']['attributesModel']['org.vadere.state.attributes.models.AttributesERM'][
        'experimentDataFilePath'] = store_path
    data['scenario']['attributesSimulation']['finishTime'] = round(max_time + 10)
    data['scenario']['topography']['attributes']['bounds']['width'] = max_x + 1
    data['scenario']['topography']['attributes']['bounds']['height'] = max_y + 1

    # create target zone
    if target_cords:
        data['scenario']['topography']['targets'] = [{
            "id": -1,
            "absorbing": False,
            "shape": {
                "x": target_cords[0],
                "y": target_cords[1],
                "width": target_cords[2],
                "height": target_cords[3],
                "type": "RECTANGLE"
            },
            "waitingTime": 0.0,
            "waitingTimeYellowPhase": 0.0,
            "parallelWaiters": 0,
            "individualWaiting": True,
            "deletionDistance": 0.1,
            "startingWithRedLight": False,
            "nextSpeed": -1.0
        }]

    with open(new_path, 'w') as outfile:
        json.dump(data, outfile)


def transform_experiment_data(path: str, store_path: str, name: str = '', create_target: bool = False,
                              header_list: [] = None, time_factor: float = None, separator: str = ',') -> None:
    assert os.path.isfile(path), f'Transforming jülich failed, bc "{path}" is no file!'
    header_list = header_list or [TIME, PED_ID, X, Y]
    assert all(elem in [TIME, PED_ID, X, Y] for elem in header_list), 'Header list is missing needed columns'
    time_factor = time_factor or TIME_FACTOR_TO_SEC

    df = pd.read_csv(path, names=header_list, sep=separator)

    # adjust time
    df[TIME] = df[TIME] * time_factor
    first_appearance = df[TIME].min()
    df[TIME] = df[TIME] - first_appearance + 1

    # adjust coordinates to meters
    df[X] = df[X] / 100
    df[Y] = df[Y] / 100

    # adjust coordinates to non negative
    if df[X].min() <= 0.1:
        df[X] = df[X] + abs(df[X].min()) + 1

    df_min = df[Y].min()

    if df[Y].min() <= 0.1:
        df[Y] = df[Y] + abs(df[Y].min()) + 1

    print(f'Experiment Length in secs: {df[TIME].max()}')
    print(f'Number of pedestrians: {df[PED_ID].nunique()}')
    print(f'X-Cord Range: {df[X].min()} bis {df[X].max()}')
    print(f'Y-Cord Range: {df[Y].min()} bis {df[Y].max()}')

    # create one df for each ped
    ped_dfs = []
    for ped_id in df[PED_ID].unique():
        ped_dfs.append(df[df[PED_ID] == ped_id])
    print('Ped dfs splitted')

    ped_dfs_transformed = []
    with click.progressbar(ped_dfs, label='Transformed Peds: ') as bar:
        for ped_df in bar:
            ped_df = ped_df.sort_values(TIME).reset_index()
            new_df = pd.DataFrame(columns=['pedestrianId', 'simTime', 'endTime', 'startX', 'startY', 'endX', 'endY'])
            for i in range(1, len(ped_df.index), STEPS):
                # splits ped df into parts and creates out of each one trajectory
                temp_df = ped_df.iloc[i - 1:i + STEPS]
                row_dict = change_df_to_traj(temp_df)
                new_df = new_df.append(row_dict, ignore_index=True)
            ped_dfs_transformed.append(new_df)

    print('Peds transformed')

    if create_target:
        x_min = 100
        x_max = 0
        y_min = 100
        y_max = 0
        for ped_df in ped_dfs_transformed:
            last_row = ped_df.iloc[-1]
            if x_min > last_row['endX']:
                x_min = last_row['endX']
            if x_max < last_row['endX']:
                x_max = last_row['endX']
            if y_min > last_row['endY']:
                y_min = last_row['endY']
            if y_max < last_row['endY']:
                y_max = last_row['endY']
        assert x_min <= x_max and y_min <= y_max
        target_cords = [x_min, y_min, x_max-x_min, y_max-y_min]
    else:
        target_cords = None

    result = pd.concat(ped_dfs_transformed)
    result = result.astype({PED_ID: 'int32'})

    os.makedirs(store_path, exist_ok=True)
    store_file_path = os.path.join(store_path, f'{name}.csv')
    create_scenario_file(result['endTime'].max(), df[X].max(), df[Y].max(), store_file_path, target_cords)

    result.to_csv(store_file_path, sep=' ', index=False, header=True, float_format='%.2f')

    # create vadere project file
    scenario_project_file_path = os.path.join(store_path, 'vadere.project')
    if not os.path.isfile(scenario_project_file_path):
        with open(scenario_project_file_path, 'w') as project_file:
            project_file.write('jülich experiments')


if __name__ == '__main__':
    path1 = '/Users/q494729/Downloads/RoughCollisionAvoidanceTrajectories/1_Trajectories_1Ped_1Obstacle.dat'
    path2 = '/Users/q494729/Downloads/RoughCollisionAvoidanceTrajectories/2_Trajectories_2Ped.dat'
    path3 = '/Users/q494729/Downloads/RoughCollisionAvoidanceTrajectories/3_Trajectories_+2Ped.dat'

    t_junction = '/Users/q494729/Downloads/KO/ko-240-240-240/ko-240-240-240_combined_MB.txt'
    t_junction_50 = '/Users/q494729/Downloads/KO/ko-240-050-240/ko-240-050-240_combined_MB.txt'

    t_junction_header = [PED_ID, TIME, X, Y, 'z']
    t_junction_time = 1.0/16
    t_junction_sep = ' '

    store_path = os.path.join(get_project_path(), 'scenarios')
    transform_experiment_data(t_junction_50, store_path, 't_junction_240_50', True, header_list=t_junction_header, time_factor=t_junction_time, separator=t_junction_sep)
