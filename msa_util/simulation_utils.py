import os
import subprocess
from typing import Tuple, List

import pandas as pd

from msa_util.names import DEFAULT_HEADER, DEFAULT_SIM_HEADER, DEFAULT_ERROR_HEADER
from msa_util.statisical_analysis.join_data import join_error_dfs, join_sim_dfs
from msa_util.helpers import progress_bar
from msa_util.msa_exceptions import ScriptError
from msa_util.file_utils.scenario_utils import change_scenario_to_temp_file, get_scenario_dict, store_scenario_file
from msa_util.file_utils.path_getters import get_vadere_jar_path, get_temp_dir, get_project_path


def run_simulation_process(output_path: str, scenario_path: str, number_of_runs: int = 10,
                           use_project_jar: bool = True) -> None:
    for i in progress_bar(list(range(number_of_runs))):
        res = subprocess.call(
            ["java", "-jar", get_vadere_jar_path(use_project_jar),
             "suq", "--output-dir", output_path + f'/{i}/', "--scenario-file", scenario_path],
            stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
        )
        if res != 0:
            raise ScriptError(f'Scenario {i} failed with status code {res}')


def run_simulation(scenario_path: str, number_of_runs: int = 10, sim_step_length: float = None,
                   use_project_jar: bool = True) -> str:
    temp_scenario_path = change_scenario_to_temp_file(scenario_path, sim_step_length=sim_step_length)
    output_path = get_temp_dir()

    run_simulation_process(scenario_path=temp_scenario_path, output_path=output_path,
                           number_of_runs=number_of_runs, use_project_jar=use_project_jar)
    return output_path


def get_data_from_simulation(output_path: str) -> Tuple[
    List[pd.DataFrame], List[pd.DataFrame], List[pd.DataFrame], List[str]]:
    trays = []
    trays_sim = []
    errors = []
    scenario_paths = []

    for dirpath, dirnames, filenames in os.walk(output_path):
        if not filenames:
            continue

        tray_file_path = os.path.join(dirpath, 'postvis.traj')
        tray_df = pd.read_csv(tray_file_path, delimiter=' ')
        tray_df.columns = DEFAULT_HEADER
        trays.append(tray_df)

        tray_sim_file_path = os.path.join(dirpath, 'simtraj.csv')
        tray_sim = pd.read_csv(tray_sim_file_path, delimiter=' ')
        tray_sim.columns = DEFAULT_SIM_HEADER
        trays_sim.append(tray_sim)

        error_path = os.path.join(dirpath, 'simerror.csv')
        error = pd.read_csv(error_path, delimiter=' ')
        error.columns = DEFAULT_ERROR_HEADER
        errors.append(error)

        for filename in filenames:
            if filename.endswith('.scenario'):
                scenario_path = os.path.join(dirpath, filename)
                scenario_paths.append(scenario_path)

    return trays, trays_sim, errors, scenario_paths


def simulate_and_create_msa_data(scenario_path: str, output_path: str, number_of_runs: int = 10,
                                 sim_step_length: float = None, use_project_jar: bool = True) -> str:
    """Overwrites old files"""

    # test input
    get_scenario_dict(scenario_path)
    if not os.path.isdir(output_path):
        raise ScriptError(f'"{output_path}" is no valid folder')

    raw_sim_data_output_path = run_simulation(scenario_path, use_project_jar=use_project_jar,
                                              number_of_runs=number_of_runs,
                                              sim_step_length=sim_step_length)

    # stores data
    trays, trays_sim, errors, scenario_paths = get_data_from_simulation(raw_sim_data_output_path)
    err = join_error_dfs(errors)
    sims = join_sim_dfs(trays_sim)
    sims.to_csv(os.path.join(output_path, 'mean_traj_data.csv'), sep=' ', index=False, header=True, float_format='%.6f')
    err.to_csv(os.path.join(output_path, 'mean_error.csv'), sep=' ', index=False, header=True, float_format='%.6f')

    # stores used scenario file
    store_scenario_file(store_path=os.path.join(output_path, 'scenario_file.scenario'),
                        data=get_scenario_dict(scenario_paths[0]))
    return output_path


if __name__ == '__main__':
    get_project_path()
    scen_path = os.path.join(get_project_path(), 'example_data/analysis_showcase/scenario_file.scenario')
    out_path = os.path.join(get_project_path(), 'example_data/analysis_showcase/5_lowered_ped_repellence')
    simulate_and_create_msa_data(scen_path, out_path, use_project_jar=True, number_of_runs=20)
    print('finished')
