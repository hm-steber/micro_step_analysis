from __future__ import annotations

import math
from itertools import chain
from typing import Optional, List, Tuple

import pandas as pd
import numpy as np

from msa_util.trajectories_utils.trajectory import FootStep, Trajectory, Scenario
from msa_util.names import TIME_START, PED_ID, TIME_END, X_START, X_END, Y_START, Y_END, DEFAULT_HEADER, SIM_TIME, \
    DATA_TYPE, Y_SIM, X_SIM, X_END_MEAN, Y_END_MEAN, DEFAULT_ERROR_HEADER, DEFAULT_MEAN_SIM_HEADER


class ErrorFootStep(FootStep):
    def __init__(self, start_time: float, end_time: float, x_start: float, x_end: float,
                 y_start: float, y_end: float, x_sim: float, y_sim: float):
        self.x_sim: float = x_sim
        self.y_sim: float = y_sim
        super(ErrorFootStep, self).__init__(start_time, end_time, x_start, x_end, y_start, y_end)

    def get_error_length(self):
        return math.sqrt((self.x_sim - self.x_start) ** 2 + (self.y_sim - self.y_start) ** 2)

    def get_error(self):
        return math.sqrt((self.x_sim - self.x_end) ** 2 + (self.y_sim - self.y_end) ** 2)

    @classmethod
    def from_df(cls, df: pd.DataFrame) -> ErrorFootStep:
        assert len(df) == 2
        assert df[TIME_START].iloc[0] == df[TIME_START].iloc[1]
        assert df[TIME_END].iloc[0] == df[TIME_END].iloc[1]
        assert df[X_START].iloc[0] == df[X_START].iloc[1]
        assert df[Y_START].iloc[0] == df[Y_START].iloc[1]
        return cls(df[TIME_START].iloc[0], df[TIME_END].iloc[0], df[X_START].iloc[0], df[X_END].iloc[0],
                   df[Y_START].iloc[0], df[Y_END].iloc[0], df[X_END].iloc[1], df[Y_END].iloc[1])

    @classmethod
    def from_series(cls, s: pd.Series):
        return cls(s[TIME_START], s[TIME_END], s[X_START], s[X_END], s[Y_START], s[Y_END], s[X_END_MEAN], s[Y_END_MEAN])


class ErrorTrajectory(Trajectory):
    def __init__(self, footsteps: List[ErrorFootStep], ped_id: int = -1, name: str = None):
        super().__init__(footsteps, ped_id, name)
        self.id = ped_id
        self.footsteps: List[ErrorFootStep] = footsteps
        self.name: Optional[str] = name

    @classmethod
    def create_from_df(cls, df: pd.DataFrame) -> ErrorTrajectory:
        food_steps = []
        ped_df = df.sort_values([SIM_TIME, DATA_TYPE])

        for i, g in ped_df.groupby(np.arange(len(ped_df)) // 2):
            food_steps.append(ErrorFootStep.from_df(g))

        traj = cls(food_steps, ped_df[PED_ID].iloc[0])
        traj.sort()
        traj.validate()
        return traj

    @classmethod
    def create_from_single_line_df(cls, df: pd.DataFrame) -> ErrorTrajectory:
        food_steps = []
        ped_df = df.sort_values([SIM_TIME])

        for i, row in ped_df.iterrows():
            food_steps.append(ErrorFootStep.from_series(row))

        traj = cls(food_steps, ped_df[PED_ID].iloc[0])
        traj.sort()
        traj.validate()
        return traj

    @classmethod
    def create_list_from_csv(cls, csv_path: str,
                             sep: str = ' ', header: List[str] = DEFAULT_MEAN_SIM_HEADER, has_header=True) -> List[ErrorTrajectory]:
        if has_header:
            skip_rows = [0]
        else:
            skip_rows = []

        df = pd.read_csv(csv_path, names=header, sep=sep, skiprows=skip_rows)
        trajs = []
        for ped_id in df[PED_ID].unique():
            trajs.append(cls.create_from_single_line_df(df[df[PED_ID] == ped_id]))
        trajs.sort(key=lambda p: p.id)

        return trajs

    @classmethod
    def create_multi_from_df(cls, df: pd.DataFrame) -> List[ErrorTrajectory]:
        # noinspection PyTypeChecker
        return super(ErrorTrajectory, cls).create_multi_from_df(df)

    def get_error_x_y(self, start_time: float = 0, end_time: float = 10000) -> Tuple[List[float], List[float]]:
        x = list(chain.from_iterable((fs.x_start, fs.x_sim, None) for fs in self if start_time <= fs.start_time <= end_time))
        y = list(chain.from_iterable((fs.y_start, fs.y_sim, None) for fs in self if start_time <= fs.start_time <= end_time))
        return x, y

    def get_error_with_cords(self) -> Tuple[List[float], List[float], List[float]]:
        x = [fs.x_start for fs in self]
        y = [fs.y_start for fs in self]
        err = [fs.get_error() for fs in self]
        return x, y, err

    def __iter__(self):
        return iter(self.footsteps)

    def to_df(self) -> pd.DataFrame:
        traj_dict = {
            PED_ID: [self.id] * len(self.footsteps),
            TIME_START: [fs.start_time for fs in self],
            TIME_END: [fs.end_time for fs in self],
            X_START: [fs.x_start for fs in self],
            Y_START: [fs.y_start for fs in self],
            X_END: [fs.x_end for fs in self],
            Y_END: [fs.y_end for fs in self],
            X_SIM: [fs.x_sim for fs in self],
            Y_SIM: [fs.y_sim for fs in self]
        }
        df = pd.DataFrame(traj_dict)
        return df


class ErrorScenario(Scenario):
    # noinspection PyMissingConstructor
    def __init__(self, scenario_path: str, trajectories: List[ErrorTrajectory] = None,
                 trajectory_df: pd.DataFrame = None):
        self.scenario_path = scenario_path
        if trajectories:
            self.trajectories = trajectories
        else:
            self.trajectories = Trajectory.create_multi_from_df(trajectory_df)

    def get_errors_with_cords(self) -> Tuple[List[float], List[float], List[float]]:
        x = []
        y = []
        err = []
        for traj in self:
            x_temp, y_temp, err_temp = traj.get_error_with_cords()
            x.extend(x_temp)
            y.extend(y_temp)
            err.extend(err_temp)
        return x, y, err
