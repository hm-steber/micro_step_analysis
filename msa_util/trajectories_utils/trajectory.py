from __future__ import annotations

import json
import math
from dataclasses import dataclass
from typing import List, Tuple, Optional
import pandas as pd

from msa_util.msa_exceptions import ScriptError
from msa_util.names import TIME_START, PED_ID, TIME_END, X_START, X_END, Y_START, Y_END, DEFAULT_HEADER, X_SIM, Y_SIM


@dataclass
class FootStep:
    start_time: float
    end_time: float
    x_start: float
    x_end: float
    y_start: float
    y_end: float

    def length(self) -> float:
        return math.sqrt((self.x_end - self.x_start) ** 2 + (self.y_end - self.y_start) ** 2)

    def time_length(self) -> float:
        return self.end_time - self.start_time

    def __str__(self):
        return f'FootStep:[{self.start_time}-{self.end_time}, {self.x_start}-{self.x_end}, {self.y_start}-{self.y_end}]'

    @classmethod
    def create_from_series(cls, s: pd.Series):
        return cls(s[TIME_START], s[TIME_END], s[X_START], s[X_END], s[Y_START], s[Y_END])


class Trajectory:
    def __init__(self, footsteps: List[FootStep], ped_id: int = -1, name: str = None):
        self.id = ped_id
        self.footsteps: List[FootStep] = footsteps
        self.name: Optional[str] = name

    def sort(self) -> Trajectory:
        self.footsteps.sort(key=lambda f: f.start_time)
        return self

    def validate(self) -> bool:
        some_not_valid = False
        for i, footstep in enumerate(self.footsteps[:-1]):
            time_is_valid = math.isclose(footstep.end_time, self.footsteps[i + 1].start_time, rel_tol=1e-5)
            x_is_valid = math.isclose(footstep.x_end, self.footsteps[i + 1].x_start, rel_tol=1e-5)
            y_is_valid = math.isclose(footstep.y_end, self.footsteps[i + 1].y_start, rel_tol=1e-5)
            if not (time_is_valid and x_is_valid and y_is_valid):
                some_not_valid = True
                print(f'{footstep} to {self.footsteps[i]} is not valid')
        if some_not_valid:
            raise ScriptError('Some foot steps are not valid')

        return True

    def get_index_span(self, start: int, end: int):
        new_footsteps = []

        def mirror(x, y, x_mirror, y_mirror):
            return x + 2 * (x_mirror - x), y + 2 * (y_mirror - y)
        if start < 0:
            # mirrors first points to make up for not existing
            x_m, y_m = self.footsteps[0].x_start, self.footsteps[0].y_start
            mirrored_points = [(x_m, y_m)]
            for i in range(1, abs(start)+1):
                fs = self.footsteps[i]
                mirrored_points.append(mirror(fs.x_start, fs.y_start, x_m, y_m))
            for i, p in enumerate(mirrored_points[:-1]):
                p_next = mirrored_points[i+1]
                new_footsteps.append(FootStep(0, 0, x_start=p_next[0], y_start=p_next[1], x_end=p[0], y_end=p[1]))
        start = 0 if start < 0 else start
        list_end = len(self.footsteps) if end > len(self.footsteps) else end
        new_footsteps.extend(self.footsteps[start:list_end])
        if end > len(self.footsteps):
            # mirrored the last points
            x_m, y_m = self.footsteps[-1].x_end, self.footsteps[-1].y_end
            x_tmp, y_tmp = x_m, y_m
            for neg_index in range(-1, (len(self.footsteps)-end)-1, -1):
                fs = self.footsteps[neg_index]
                x, y = mirror(fs.x_start, fs.y_start, x_m, y_m)
                new_footsteps.append(FootStep(0, 0, x_start=x_tmp, y_start=y_tmp, x_end=x, y_end=y))
                x_tmp, y_tmp = x, y

        return Trajectory(new_footsteps, self.id)

    def get_time_length(self):
        time_length = 0.0
        for f in self:
            time_length += f.time_length()
        return time_length

    def get_length(self):
        length = 0.0
        for f in self:
            length += f.length()
        return length

    def get_average_speed(self):
        return self.get_length() / float(self.get_time_length())

    def get_x_y(self, start_time: float = 0, end_time: float = 10000) -> Tuple[List[float], List[float]]:
        matching_fs = [fs for fs in self if start_time <= fs.start_time <= end_time]
        x = [fs.x_start for fs in matching_fs]
        y = [fs.y_start for fs in matching_fs]
        if matching_fs:
            x.append(matching_fs[-1].x_end)
            y.append(matching_fs[-1].y_end)
        return x, y

    def __iter__(self):
        return iter(self.footsteps)

    def __str__(self):
        average_speed = round(self.get_length() / self.get_time_length(), 2)
        return f'Pedestrian {self.id} with length: {round(self.get_length(), 2)}m,' \
               f' timelength: {round(self.get_time_length(), 2)}s and average speed: {average_speed}m/s'

    @classmethod
    def create_from_df(cls, df: pd.DataFrame) -> Trajectory:
        assert df[PED_ID].nunique() == 1
        footsteps = [FootStep.create_from_series(row) for index, row in df.iterrows()]
        traj = cls(footsteps, df[PED_ID].unique()[0])
        traj.sort()
        traj.validate()
        return traj

    @classmethod
    def create_multi_from_df(cls, df: pd.DataFrame) -> List[Trajectory]:
        trajs = []
        for ped_id in df[PED_ID].unique():
            trajs.append(cls.create_from_df(df[df[PED_ID] == ped_id]))
        trajs.sort(key=lambda p: p.id)
        return trajs

    @classmethod
    def create_from_csv(cls, csv_path: str,
                        sep: str = ' ', header: List[str] = DEFAULT_HEADER, has_header=True) -> List[Trajectory]:
        if has_header:
            skip_rows = [0]
        else:
            skip_rows = []

        df = pd.read_csv(csv_path, names=header, sep=sep, skiprows=skip_rows)
        return cls.create_multi_from_df(df)

    def smooth_trajectory(self: Trajectory, n: int = 11, name: str = None) -> Trajectory:
        new_footsteps = []
        for i, foot_step in enumerate(self):
            relevant_trajectory = self.get_index_span(i - n, i + n + 1)
            x_start = sum([f.x_start for f in relevant_trajectory]) / len(relevant_trajectory.footsteps)
            y_start = sum([f.y_start for f in relevant_trajectory]) / len(relevant_trajectory.footsteps)
            x_end = sum([f.x_end for f in relevant_trajectory]) / len(relevant_trajectory.footsteps)
            y_end = sum([f.y_end for f in relevant_trajectory]) / len(relevant_trajectory.footsteps)
            new_footstep = FootStep(foot_step.start_time, foot_step.end_time, x_start=x_start, y_start=y_start,
                                    x_end=x_end, y_end=y_end)
            new_footsteps.append(new_footstep)
        return Trajectory(new_footsteps, self.id, name or self.name)

    def to_df(self) -> pd.DataFrame:
        traj_dict = {
            PED_ID: [self.id] * len(self.footsteps),
            TIME_START: [fs.start_time for fs in self],
            TIME_END: [fs.end_time for fs in self],
            X_START: [fs.x_start for fs in self],
            Y_START: [fs.y_start for fs in self],
            X_END: [fs.x_end for fs in self],
            Y_END: [fs.y_end for fs in self]
        }
        df = pd.DataFrame(traj_dict)
        return df

    def save(self, path: str) -> None:
        df = self.to_df()
        df.to_csv(path, sep=' ', index=False, header=True, float_format='%.2f')

    def first_appearance(self):
        return self.footsteps[0].start_time

    def last_appearance(self):
        return self.footsteps[-1].end_time


class Scenario:
    def __init__(self, scenario_path: str, data_path: str = None, trajectories: List[Trajectory] = None):
        self.scenario_path = scenario_path

        if not data_path and trajectories is None:
            with open(scenario_path) as json_file:
                scenario_dict = json.load(json_file)
            data_path = \
                scenario_dict['scenario']['attributesModel']['org.vadere.state.attributes.models.AttributesERM'][
                    'experimentDataFilePath']
        self.trajectories: List[Trajectory] = trajectories or Trajectory.create_from_csv(data_path)

    @classmethod
    def from_trajectories(cls, scenario_path: str, trajectories: List[Trajectory]):
        return cls(scenario_path=scenario_path, trajectories=trajectories)

    @classmethod
    def from_scenario(cls, scenario_path: str, ):
        return cls(scenario_path=scenario_path)

    def get_part(self, ids: List[int] or int) -> Scenario:
        if isinstance(ids, int):
            ids = [ids]
        relevant_trajectories = [traj for traj in self if traj.id in ids]
        return self.from_trajectories(scenario_path=self.scenario_path, trajectories=relevant_trajectories)

    def get_ids(self):
        return [traj.id for traj in self]

    def showcase_smoothed(self, n: int) -> Scenario:
        new_trajs = self.trajectories.copy()
        new_trajs.extend(
            [traj.smooth_trajectory(n=n, name=f'Smoothed Trajectory Pedestrian{traj.id}') for traj in self])
        return Scenario.from_trajectories(self.scenario_path, new_trajs)

    def smooth(self, n: int) -> Scenario:
        new_trajs = [traj.smooth_trajectory(n=n, name=f'Smoothed Trajectory Pedestrian{traj.id}') for traj in self]
        return Scenario.from_trajectories(self.scenario_path, new_trajs)

    def to_df(self) -> pd.DataFrame:
        traj_dfs = [traj.to_df() for traj in self]
        result = pd.concat(traj_dfs)
        return result

    def save(self, path: str) -> None:
        self.to_df().to_csv(path, sep=' ', index=False, header=True, float_format='%.2f')

    def __iter__(self):
        return iter(self.trajectories)

    def __str__(self):
        return f'Scenario with {len(self.trajectories)} pedestrians'


if __name__ == '__main__':
    trajs = Trajectory.create_from_csv('/Users/q494729/IdeaProjects/micro_step_analysis/scenarios/big_one.csv')
    print(trajs[0].get_x_y())
